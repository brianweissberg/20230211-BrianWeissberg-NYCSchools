//
//  SchoolsServiceManagerTests.swift
//  20230211-BrianWeissberg-NYCSchoolsTests
//
//  Created by Brian Weissberg on 2/13/23.
//

import XCTest
@testable import NY_Schools

/// Validates Schools Service manager handles different cases of JSON. (Success, Emtpy and Malformed)
class ServiceManagerTests: XCTestCase {
    
    // MARK: - SchoolsServiceManager
    
    /// Using valid JSON, test `SchoolsServiceManager.parseSchools` to ensure it will serialize school data successfully
    func testParseSchoolsSuccessJson() {
        guard let jsonString = TestHelper.fetchDirectoryMockJson(.successSchoolsDirectory) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect success parse of data")
        let data = Data(jsonString.utf8)
        
        SchoolsServiceManager().parseSchools(data: data) { result in
            switch result {
            case .success(_):
                expectation.fulfill()
            case .failure(_):
                XCTFail("Expected valid json to be serialized")
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }

    /// Using invalid JSON, test `SchoolsServiceManager.parseSchools` to ensure that invalid JSON returns failure.
    func testParseSchoolsInvalidJson() {
        guard let jsonString = TestHelper.fetchDirectoryMockJson(.malformed) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect failure when parsing data")
        let data = Data(jsonString.utf8)
        SchoolsServiceManager().parseSchools(data: data) { result in
            switch result {
            case .success(_):
                XCTFail("Expect to not serialize malformed JSON")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, "Successfully fetched data but unable to serialize schools data")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }

    /// Using  **empty** JSON, test `SchoolsServiceManager.parseSchools` to ensure that it returns failure.
    func testParseSchoolsEmptyJson() {
        guard let jsonString = TestHelper.fetchDirectoryMockJson(.empty) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect to be able to serialize json")
        let data = Data(jsonString.utf8)
        SchoolsServiceManager().parseSchools(data: data) { result in
            switch result {
            case .success(_):
                XCTFail("JSON is empty and if no school data can be parsed from the empty json, then should fail")
            case .failure(_):
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }

    // MARK: - SchoolsServiceManager

    /// Using valid JSON, test `SchoolsServiceManager.parseSchoolDetails` to ensure it will serialize successfully
    func testParseSchoolDetailsSuccessJson() {
        guard let jsonString = TestHelper.fetchDetailsMockJson(.successSchoolsDetails) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect success parse of data")
        let data = Data(jsonString.utf8)
        
        SchoolsServiceManager().parseSchoolDetails(data: data) { result in
            switch result {
            case .success(_):
                expectation.fulfill()
            case .failure(_):
                XCTFail("Expected valid json to be serialized")
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }

    /// Using invalid JSON, test `SchoolsServiceManager.parseSchoolDetails` to ensure that invalid JSON returns failure.
    func testParseSchoolDetailsInvalidJson() {
        guard let jsonString = TestHelper.fetchDetailsMockJson(.malformed) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect failure when parsing data")
        let data = Data(jsonString.utf8)
        SchoolsServiceManager().parseSchoolDetails(data: data) { result in
            switch result {
            case .success(_):
                XCTFail("Expect to not serialize malformed JSON")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, "Successfully fetched data but unable to serialize school details data")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }

    /// Using  **empty** JSON, test `SchoolsServiceManager.parseSchoolDetails` to ensure that it returns failure.
    func testParseSchoolDetailsEmptyJson() {
        guard let jsonString = TestHelper.fetchDetailsMockJson(.empty) else {
            XCTFail("JSON not found")
            return
        }
        let expectation = XCTestExpectation(description: "Expect to be able to serialize json")
        let data = Data(jsonString.utf8)
        SchoolsServiceManager().parseSchoolDetails(data: data) { result in
            switch result {
            case .success(_):
                XCTFail("JSON is empty and if no school data can be parsed from the empty json, then should fail")
            case .failure(_):
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 2.0)
    }
}

