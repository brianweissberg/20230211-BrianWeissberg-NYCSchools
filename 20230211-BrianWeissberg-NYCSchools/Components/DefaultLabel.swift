//
//  DefaultLabel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import UIKit

class DefaultLabel: UILabel {
    init(font: UIFont = .systemFont(ofSize: 17),
         textColor: UIColor,
         identifier: DefaultLabelAccessibliltyIdentifiers) {
        super.init(frame: .zero)
        self.numberOfLines = 0
        self.font = font
        self.textColor = textColor
        self.sizeToFit()
        self.accessibilityIdentifier = identifier.rawValue
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}
