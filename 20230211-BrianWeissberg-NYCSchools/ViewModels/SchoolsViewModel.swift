//
//  SchoolsViewModel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

protocol SchoolViewModelDelegate: AnyObject {
    func parseSchoolsFailure(_ error: Error)
}

/// Manages data for school directory and school details
class SchoolsViewModel {
    let dataProvider: SchoolsDataProviding
    let networkManager: NetworkManaging
    let serviceManager: SchoolsServiceManaging
    weak var delegate: SchoolViewModelDelegate?
    
    init(dataProvider: SchoolsDataProviding, networkManager: NetworkManaging = NetworkManager(), serviceManager: SchoolsServiceManaging = SchoolsServiceManager(), delegate: SchoolViewModelDelegate? = nil) {
        self.dataProvider = dataProvider
        self.networkManager = networkManager
        self.serviceManager = serviceManager
        self.delegate = delegate
    }
    
    func fetchSchoolsData(_ completion: @escaping () -> Void) {
        guard let url = URL(string: NetworkUrl.schoolDirectory.rawValue) else {
            delegate?.parseSchoolsFailure(SchoolsServiceError.invalidSchoolsUrl)
            return
        }
        
        networkManager.fetchData(url) { result in
            switch result {
            case .success(let data):
                self.serviceManager.parseSchools(data: data) { result in
                    switch result {
                    case .success(let schools):
                        self.dataProvider.handleSchools(schools) {
                            completion()
                        }
                    case .failure(let error):
                        self.delegate?.parseSchoolsFailure(error)
                    }
                }
            case .failure(let error):
                self.delegate?.parseSchoolsFailure(error)
            }
        }
    }

    func fetchSchoolDetailsData(_ completion: (() -> Void)? = nil) {
        guard let url = URL(string: NetworkUrl.schoolDetails.rawValue) else {
            delegate?.parseSchoolsFailure(SchoolsServiceError.invalidSchoolsUrl)
            return
        }
        
        networkManager.fetchData(url) { result in
            switch result {
            case .success(let data):
                self.serviceManager.parseSchoolDetails(data: data) { result in
                    switch result {
                    case .success(let schoolDetails):
                        self.dataProvider.handleSchoolDetails(schoolDetails)
                        completion?()
                    case .failure(let error):
                        self.delegate?.parseSchoolsFailure(error)
                    }
                }
            case .failure(let error):
                self.delegate?.parseSchoolsFailure(error)
            }
        }
    }
}

