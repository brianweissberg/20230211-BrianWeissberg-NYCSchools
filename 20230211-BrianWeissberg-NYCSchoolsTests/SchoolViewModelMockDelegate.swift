//
//  SchoolViewModelMockDelegate.swift
//  20230211-BrianWeissberg-NYCSchoolsTests
//
//  Created by Brian Weissberg on 2/13/23.
//

import XCTest
@testable import NY_Schools

final class SchoolsViewModelMockDelegate: SchoolViewModelDelegate {
    let expectation = XCTestExpectation(description: "Expect parse schools failure")
    func parseSchoolsFailure(_ error: Error) {
        expectation.fulfill()
    }
}
