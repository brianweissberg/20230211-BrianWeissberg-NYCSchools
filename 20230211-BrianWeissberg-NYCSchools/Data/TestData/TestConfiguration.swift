//
//  TestConfiguration.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/12/23.
//

import Foundation

enum UiTestMode: String {
    case successSchoolDirectoryNoDelay = "Success Schools Directory - No Delay"
    case successSchoolDirectoryThreeSecDelay = "Success Schools Directory - Three Second Delay"
    case failedNetworkNoDelay = "Failed Network Call - No Delay"
    case failedNetworkTwoSecDelay = "Failed Network Call = Two Second Delay"
}

/// Controls the setup of the app for UI testing. When each UI test begins, it injects a `launchEnvironment` string which is mapped to a configuation setup in this calls. This allows for the same app to be used with real data and mock data.
class TestConfiguration {
    let isTesting: Bool
    let networkCallShouldFail: Bool
    let directoryMockJson: DirectoryMockJson
    let detailsMockJson: DetailsMockJson
    let delay: Double
    
    init(isTesting: Bool = true, networkCallShouldFail: Bool = false, directoryMockJson: DirectoryMockJson = .successSchoolsDirectory, detailsMockJson: DetailsMockJson = .successSchoolsDetails, delay: Double = 0) {
        self.isTesting = isTesting
        self.networkCallShouldFail = networkCallShouldFail
        self.directoryMockJson = directoryMockJson
        self.detailsMockJson = detailsMockJson
        self.delay = delay
    }

    static func getTestConfiguration() -> TestConfiguration {
        let defaultConfiguration = TestConfiguration(isTesting: false)
        guard let testMode = ProcessInfo().environment["testMode"] else { return defaultConfiguration }
        switch testMode {
        case UiTestMode.successSchoolDirectoryNoDelay.rawValue:
            return TestConfiguration()
        case UiTestMode.successSchoolDirectoryThreeSecDelay.rawValue:
            return TestConfiguration(delay: 3.0)
        case UiTestMode.failedNetworkNoDelay.rawValue:
            return TestConfiguration(networkCallShouldFail: true)
        case UiTestMode.failedNetworkTwoSecDelay.rawValue:
            return TestConfiguration(networkCallShouldFail: true, delay: 2.0)
        default:
            return defaultConfiguration
        }
    }
}
