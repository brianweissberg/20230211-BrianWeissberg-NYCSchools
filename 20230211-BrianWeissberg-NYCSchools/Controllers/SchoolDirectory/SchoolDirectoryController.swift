//
//  SchoolDirectoryController.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import UIKit
import Foundation

/// Displays a TableView of all schools with a search bar.
class SchoolDirectoryController: UIViewController, UISearchBarDelegate {
    private let coordinator: MainCoordinating
    private let viewModel: SchoolsViewModel
    private let tableView: SchoolDirectoryTableView
    private lazy var searchController: UISearchController = {
        let controller = SchoolDirectorySearchBarController(controllerDelegate: self, searchBarDelegate: self, searchResultsUpdater: self, controller: self)
        navigationItem.searchController = controller
        navigationItem.hidesSearchBarWhenScrolling = true
        return controller
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // The loading indictor needs to be added to the view once subviews are laid out as Activity Inidcator must be in the center of the view.
        handleLoadingIndicator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Setting up navigation title here because we want to most current state of the view to be determined before setting up navigation title. If setup is done earlier in view lifecyle (i.e. viewDidLoad), it's possible that the data could load before view appears and navigation title would be incorrectly configured.
        handleNavigation()
    }
    
    init(viewModel: SchoolsViewModel, coordinator: MainCoordinating) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        self.tableView = SchoolDirectoryTableView()
        super.init(nibName: nil, bundle: nil)
        setUpTableView()
        tableView.dataSource = self
        tableView.delegate = self
        handleSearchBar()
        fetchData()
    }

    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}

// - MARK: TableView DataSource and Delegate

extension SchoolDirectoryController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataProvider.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewConstants.cell.rawValue, for: indexPath) as? SchoolTableViewCell ?? SchoolTableViewCell()
        let school = viewModel.dataProvider.schools[indexPath.row]
        cell.configure(school, dataProvider: viewModel.dataProvider)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let school = viewModel.dataProvider.schools[indexPath.row]
        coordinator.showSchoolDetails(school: school, dataProvider: viewModel.dataProvider)
    }
}

// - MARK: LoadingIndicator

extension SchoolDirectoryController {
    /// Handles adding and removing loading indicator based on whether or not school directory data has been successfully loaded
    ///
    /// NOTE: The loading view is always present until the view loads successfully. If the UIAlert is displayed showing network error and asking the user to retry, then one user hit's retry button and the alert is dismissed, there is no chance of lag to add the loading indicator view back to the screen. This assures the user that as soon as they hit retry to fetch data again, there is no lag in showing that data is being fetched
    private func handleLoadingIndicator() {
        guard viewModel.dataProvider.allSchoolsDidFinishLoading else {
            coordinator.showSchoolDirectoryLoadingIndicator()
            return
        }
        coordinator.removeSchoolDirectoryLoadingIndicator()
        
    }
}

// - MARK: SearchBar

extension SchoolDirectoryController: UISearchControllerDelegate, UISearchResultsUpdating {
    /// Updates search results each time user interacts with the search bar
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            viewModel.dataProvider.filterSchools(searchText) {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    /// Displays the search bar if all schools have **loaded successfully**. Hides search bar if all schools have **not loaded successfully**.
    private func handleSearchBar() {
        // SHOW SEARCH BAR If all schools have finished loading.
        guard viewModel.dataProvider.allSchoolsDidFinishLoading else {
            searchController.searchBar.isHidden = true
            return
        }
        
        // HIDE SEARCH BAR if schools have not yet loaded
        searchController.searchBar.isHidden = false
    }
}

// - MARK: SetUp

extension SchoolDirectoryController {
    /// Configures view after successful fetch of school directory information. Currently there isn't callback/listener for schools details information as if that information does not exist, the details screen is still displayed but does not display any SAT score information.
    private func setUpViewAfterFetchSchoolsSuccess() {
        handleLoadingIndicator()
        handleNavigation()
        handleSearchBar()
        tableView.reloadData()
    }

    /// Handles custom action when user taps retry in UIAlert presented for network failure when fetching Schools Directory information.
    private func retryTappedNetworkFailureAlert() {
        handleLoadingIndicator()
        handleSearchBar()
        handleNavigation()
        fetchData()
    }

    private func setUpTableView() {
        view.addSubview(tableView)
        tableView.backgroundColor = .schoolDirectoryTableView
        tableView.setUpConstraints(parentView: self.view)
    }

    /// Fetches data for main schools directory and for school details.
    private func fetchData() {
        viewModel.fetchSchoolDetailsData()
        viewModel.fetchSchoolsData {
            DispatchQueue.main.async {
                self.setUpViewAfterFetchSchoolsSuccess()
            }
        }
    }
}

// - MARK: Fetch Failure

extension SchoolDirectoryController: SchoolViewModelDelegate {
    /// Handles business logic for school directory network call failed.
    func parseSchoolsFailure(_ error: Error) {
        // Introducing slight delay here so that when user taps retry in alert, they see that app is attempting to refetch data. Without the delay and with not network available, the user would tap retry and as soon as they tap it, another alert is immediately displayed. This may confuse user into thinking that because another alert was shown so quickly, the app did not actually try and fetch new data.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.handleLoadingIndicator()
            self.handleSearchBar()
            self.handleNavigation()
            let message = TableViewConstants.retryMessage.rawValue
            self.coordinator.showRetryAlert(message: message, completion: self.retryTappedNetworkFailureAlert)
        }
    }
}

// - MARK: Navigation

extension SchoolDirectoryController {
    /// Sets the color of the navigation bar and the text of the navigation title. Navigation title should not exist unless school directory information has loaded successfully.
    private func handleNavigation() {
        navigationController?.navigationBar.backgroundColor = .schoolDirectoryNavigationBar
        guard viewModel.dataProvider.allSchoolsDidFinishLoading else {
            title = nil
            return
        }
        UIAccessibility.post(notification: .screenChanged, argument: coordinator.navController.navigationBar)
        title = SchoolConstants.schools.rawValue
    }
}
