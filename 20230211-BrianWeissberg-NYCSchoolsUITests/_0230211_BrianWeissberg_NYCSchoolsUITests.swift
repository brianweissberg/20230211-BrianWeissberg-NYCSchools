//
//  _0230211_BrianWeissberg_NYCSchoolsUITests.swift
//  20230211-BrianWeissberg-NYCSchoolsUITests
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation
import XCTest

class _0230211_BrianWeissberg_NYCSchoolsUITests: XCTestCase {
    let app = XCUIApplication()
    let elements = UITestingElements()
    
    /// Validates that no loading indicator is shown to user if network call succeeds before the Schools Directory TableView is displayed. Validates Details Screen elements for a details screen that displays all possible elements. 
    func test01SuccessLaunchNoDelay() throws {
        app.launchEnvironment = ["testMode": UiTestMode.successSchoolDirectoryNoDelay.rawValue]
        app.launch()
        // App lauches without delay and immediately successfully displays school directory
        validateElementsExist(elements.directoryElements)
        elements.successSchoolCell.tap()
        // Details Screen loads successfully
        let detailsElements = elements.detailsElements
        validateElementsExist(detailsElements)
    }

    /// Validates that loading indictator is displayed for slow network requests. Once network request succeeds, then schools directory is displayed to user.
    func test02SuccessLaunchThreeSecondDelay() throws {
        app.launchEnvironment = ["testMode": UiTestMode.successSchoolDirectoryThreeSecDelay.rawValue]
        app.launch()
        // App launches with three second delay, should be displaying activity indicator
        validateElementExists(elements.loadingIndicator)
        validateElementsDoNotExist(elements.directoryElements)
        // Activity indictor will be dismissed when view loads after three seconds, validate School Directory view successfully loads
        validateElementsExist(elements.directoryElements, timeout: 4)
        validateElementDoesNotExist(elements.loadingIndicator)
    }

    /// Validates scenario where app is launced and network call fails immediately, thus loading indicator will not be displayed before inital retry alert is displayed.
    func test03FailureLaunchNoDelay() throws {
        app.launchEnvironment = ["testMode": UiTestMode.failedNetworkNoDelay.rawValue]
        app.launch()
        validateElementsExist(elements.retryAlertElements)
        validateElementsDoNotExist(elements.directoryElements)
    }

    /// Validates that with failed network call, retry alert is displayed. Validates that when user hits retry button, loading screen is displayed while network call is made and when network call fails, retry alert is displayed again. Validates this flow until retry alert is displayed three times.
    func test04FailureLaunchTwoSecondDelay() throws {
        app.launchEnvironment = ["testMode": UiTestMode.failedNetworkTwoSecDelay.rawValue]
        app.launch()
        // App launches with two second delay, should only be displaying activity indicator
        validateElementExists(elements.loadingIndicator)
        validateElementsDoNotExist(elements.directoryElements)

        // FIRST RETRY ALERT
        // App failes to fetch directory data, should display retry alert
        validateElementsExist(elements.retryAlertElements, timeout: 3)
        validateElementsDoNotExist(elements.directoryElements)
        // Hit Retry button
        elements.retryButton.tap()
        validateElementExists(elements.loadingIndicator)
        validateElementsDoNotExist(elements.directoryElements)

        // SECOND RETRY ALERT
        // App failes to fetch directory data, should display retry alert
        validateElementsExist(elements.retryAlertElements, timeout: 3)
        validateElementsDoNotExist(elements.directoryElements)
        // Hit Retry button
        elements.retryButton.tap()
        validateElementExists(elements.loadingIndicator)
        validateElementsDoNotExist(elements.directoryElements)

        // THIRD RETRY ALERT
        // App failes to fetch directory data, should display retry alert
        validateElementsExist(elements.retryAlertElements, timeout: 3)
        validateElementsDoNotExist(elements.directoryElements)
    }
}

extension _0230211_BrianWeissberg_NYCSchoolsUITests {
    private func validateElementsExist(_ elements: [XCUIElement], timeout: TimeInterval = 0) {
        for element in elements {
            self.validateElementExists(element, timeout: timeout)
        }
    }

    private func validateElementExists(_ element: XCUIElement, timeout: TimeInterval = 0) {
        let exists = element.waitForExistence(timeout: timeout)
        XCTAssertTrue(exists, "FAILED ELEMENT: \(element.debugDescription)")
    }

    private func validateElementsDoNotExist(_ elements: [XCUIElement]) {
        for element in elements {
            validateElementDoesNotExist(element)
        }
    }

    private func validateElementDoesNotExist(_ element: XCUIElement) {
        let exists = element.waitForExistence(timeout: 0)
        XCTAssertFalse(exists, "FAILED ELEMENT: \(element.debugDescription)")
    }
}
