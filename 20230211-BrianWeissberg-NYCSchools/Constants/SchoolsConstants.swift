//
//  SchoolsConstants.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

enum SchoolConstants: String {
    case schools = "New York Schools"
    case satDetails = "SAT Details"
    case noSATDetails = "No SAT Details"
    case contact = "Contact"
    case extracurricularActivities = "Extracurricular Activities"
    case searchBarPrompt = "Enter a school name"
    case loading = "Loading"
}

enum SchoolDetailsConstants: String {
    case testTakers = "Number of Test Takers - "
    case averageReadingScore = "Average Reading Score - "
    case averageWritingScore = "Average Writing Score - "
    case averageMathScore = "Average Math Score - "
    case fax = "Fax: "
    case website = "Website: "
}

enum TableViewConstants: String {
    case cell
    case retryMessage = "Unable to fetch data for schools"
    case retry = "Retry"
    case emptyString = ""
    case networkError = "Network Error"
    case initFatalError = "init(coder:) has not been implemented"
}

enum TableViewCellConstants: Int {
    case stackSpacing = 5
}

enum NetworkUrl: String {
    case schoolDirectory = "https://data.cityofnewyork.us/resource/s3k6-pzi2"
    case schoolDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

enum DefaultContainerAccessibliltyIdentifiers: String, CaseIterable {
    case detailsContentContainer = "SchoolDetailsController: Content Container"
    case detailsSchoolNameContainer = "SchoolDetailsController: School Name Container"
    case detailsOverviewContainer = "SchoolDetailsController: Overview Container"
    case detailsSatLabelsContainer = "SchoolDetailsController: SAT Labels Container"
    case detailsContactContainer = "SchoolDetailsController: Contact Container"
    case detailsExtracurricularActivitiesContainer = "SchoolDetailsController: Extracurricular Activities Container"
    case cellLabelContainer = "SchoolTableViewCell: Label Container"
    case scrollViewContainer = "SchoolDetailsController: ScrollView Container"
    case mapViewContainer = "SchoolDetailsController: MapView Container"
}

enum DefaultLabelAccessibliltyIdentifiers: String, CaseIterable {
    case cellSchoolNameLabel = "SchoolTableViewCell: School Name Label"
    case detailsSchoolNameLabel = "SchoolDetailsController: School Name Label"
    case detailsOverviewLabel = "SchoolDetailsController: Overview Label"
    case detailsSatDetailsTitleLabel = "SchoolDetailsController: SAT Details Title Label"
    case detailsNumberTestTakersLabel = "SchoolDetailsController: Number Test Takers Label"
    case detailsReadingScoreLabel = "SchoolDetailsController: Reading Score Label"
    case detailsMathScoreLabel = "SchoolDetailsController: Math Score Label"
    case detailsWritingScoreLabel = "SchoolDetailsController: Writing Score Label"
    case detailsExtracurricularActivitiesLabel = "SchoolDetailsController: Extracurricular Activities Label"
    case detailsContactTitleLabel = "SchoolDetailsController: Contact Title Label"
    case detailsFaxLabel = "SchoolDetailsController: Fax Label"
    case detailsPhoneLabel = "SchoolDetailsController: Phone Label"
    case detailsSchoolEmailLabel = "SchoolDetailsController: School Email Label"
    case detailsSchoolWebsiteLabel = "SchoolDetailsController: School Website Label"
}

enum SchoolDirectoryAccessibiltyIdentifiers: String {
    case searchBar = "Search Bar"
}

enum SchoolDetailsMapViewIdentifier: String {
    case mapViewPin = "SchoolDetailsController: MapView Pin"
}

enum AccessibilityHelper: String {
    case commaPause = ", "
    case mapViewLabel = "MapView: "
}
