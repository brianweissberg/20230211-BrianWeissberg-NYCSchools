//
//  SchoolsServiceManager.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

enum SchoolsServiceError: Error {
    case invalidSchoolsUrl
    case invalidSchoolsData
    case invalidSchoolDetailsData
}

enum SchoolsErrorConstants: String {
    case invalidSchoolsUrl = "Unable to successfully parse schools url string into Url"
    case invalidSchoolsData = "Successfully fetched data but unable to serialize schools data"
    case invalidSchoolDetailsData = "Successfully fetched data but unable to serialize school details data"
}

extension SchoolsServiceError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidSchoolsUrl:
            return SchoolsErrorConstants.invalidSchoolsUrl.rawValue
        case .invalidSchoolsData:
            return SchoolsErrorConstants.invalidSchoolsData.rawValue
        case .invalidSchoolDetailsData:
            return SchoolsErrorConstants.invalidSchoolDetailsData.rawValue
        }
    }
}

protocol SchoolsServiceManaging {
    func parseSchools(data: Data, completion: @escaping (Result<[SchoolModel], SchoolsServiceError>) -> Void)
    func parseSchoolDetails(data: Data, completion: @escaping (Result<[SchoolDetailsModel], SchoolsServiceError>) -> Void)
}

/// Parses fetched data for school directory and school details
class SchoolsServiceManager: SchoolsServiceManaging {
    func parseSchools(data: Data, completion: @escaping (Result<[SchoolModel], SchoolsServiceError>) -> Void) {
        let decoder = JSONDecoder()
        do {
            let response = try decoder.decode([SchoolModel].self, from: data)
            guard !response.isEmpty else {
                completion(.failure(SchoolsServiceError.invalidSchoolsData))
                return
            }
            completion(.success(response))
        } catch {
            completion(.failure(SchoolsServiceError.invalidSchoolsData))
        }
    }

    func parseSchoolDetails(data: Data, completion: @escaping (Result<[SchoolDetailsModel], SchoolsServiceError>) -> Void) {
        let decoder = JSONDecoder()
        do {
            let response = try decoder.decode([SchoolDetailsModel].self, from: data)
            guard !response.isEmpty else {
                completion(.failure(SchoolsServiceError.invalidSchoolDetailsData))
                return
            }
            completion(.success(response))
        } catch {
            completion(.failure(SchoolsServiceError.invalidSchoolDetailsData))
        }
    }
}
