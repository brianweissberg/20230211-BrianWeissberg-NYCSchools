//
//  UITestingElements.swift
//  20230211-BrianWeissberg-NYCSchoolsUITests
//
//  Created by Brian Weissberg on 2/15/23.
//

import XCTest

// Contains XCUIElements to be validated in UI tests. 
class UITestingElements {
    let app: XCUIApplication = XCUIApplication()
    var directoryElements: [XCUIElement] {
        let elementOne = app.navigationBars["New York Schools"]
        let elementTwo = app.searchFields["Enter a school name"]
        let elementThree = app.staticTexts[DefaultLabelAccessibliltyIdentifiers.cellSchoolNameLabel.rawValue]
        return [elementOne, elementTwo, elementThree]
    }
    var detailsElements: [XCUIElement] {
        // Labels
        let labelElementOne = app.buttons[DefaultLabelAccessibliltyIdentifiers.detailsPhoneLabel.rawValue]
        let labelElementTwo = app.buttons[DefaultLabelAccessibliltyIdentifiers.detailsSchoolEmailLabel.rawValue]
        let labelElementThree = app.staticTexts[DefaultLabelAccessibliltyIdentifiers.detailsContactTitleLabel.rawValue]
        let labelElementFour = app.staticTexts[DefaultLabelAccessibliltyIdentifiers.detailsFaxLabel.rawValue]
        let labelElementFive = app.staticTexts[DefaultLabelAccessibliltyIdentifiers.detailsSchoolWebsiteLabel.rawValue]
        // Container Views
        let containerElementOne = app.otherElements[DefaultContainerAccessibliltyIdentifiers.detailsContentContainer.rawValue]
        let containerElementTwo = app.otherElements[DefaultContainerAccessibliltyIdentifiers.detailsSchoolNameContainer.rawValue]
        let containerElementThree = app.otherElements[DefaultContainerAccessibliltyIdentifiers.detailsSatLabelsContainer.rawValue]
        let containerElementFour = app.otherElements[DefaultContainerAccessibliltyIdentifiers.detailsContactContainer.rawValue]
        let containerElementFive = app.otherElements[DefaultContainerAccessibliltyIdentifiers.detailsExtracurricularActivitiesContainer.rawValue]
        let containerElementSix = app.otherElements[DefaultContainerAccessibliltyIdentifiers.scrollViewContainer.rawValue]
        let containerElementSeven = app.otherElements[DefaultContainerAccessibliltyIdentifiers.mapViewContainer.rawValue]
        // All elements
        return [containerElementOne, containerElementTwo, containerElementThree, containerElementFour, containerElementFive, containerElementSix, containerElementSeven, labelElementOne, labelElementTwo, labelElementThree, labelElementFour, labelElementFive]
    }
    var retryAlertElements: [XCUIElement] {
        let elementOne = app.staticTexts["Network Error"]
        let elementTwo = app.staticTexts["Unable to fetch data for schools"]
        let elementThree = app.buttons["Retry"]
        return [elementOne, elementTwo, elementThree]
    }
    var loadingIndicator: XCUIElement {
        app.otherElements["Loading Indicator"]
    }
    var successSchoolCell: XCUIElement {
        app.staticTexts["TEST - Success School"]
    }
    var retryButton: XCUIElement {
        app.buttons["Retry"]
    }
}
