//
//  SchoolDetailsMapView.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/12/23.
//

import MapKit

class SchoolDetailsMapView: MKMapView {
    init(schoolName: String, latitude: Double, longitude: Double) {
        super.init(frame: .zero)
        self.setUpMapView(schoolName: schoolName, latitude: latitude, longitude: longitude)
    }
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
    
    private func setUpMapView(schoolName: String, latitude: Double, longitude: Double) {
        // SetUp
        let locationCoordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.region = MKCoordinateRegion(center: locationCoordinates, span: span)

        // Pin showing name of school
        let pin = MKPointAnnotation()
        pin.coordinate = locationCoordinates
        pin.title = schoolName
        self.addAnnotation(pin)
        
        // Adjusts camera angle for 3D effect when map is initally displayed.
        let camera = MKMapCamera()
        camera.centerCoordinate = locationCoordinates
        camera.pitch = 80
        camera.altitude = 60
        self.camera = camera
    }

    func setUpConstraints(parentView: UIView) {
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: parentView.topAnchor),
            self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: parentView.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: parentView.trailingAnchor),
        ])
    }
}
