//
//  SchoolTableViewCell.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import UIKit

protocol SchoolTableViewCellDelegate: AnyObject {
    func schoolTableViewCellTapped()
}

/// Custom TableView Cell for School Directory. Currently contains a single label displaying the name of the school.
final class SchoolTableViewCell: UITableViewCell {
    private let schoolNameLabel: DefaultLabel
    weak var delegate: SchoolTableViewCellDelegate?
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.schoolNameLabel = DefaultLabel(textColor: .schoolsListViewText, identifier: .cellSchoolNameLabel)
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }

    override func prepareForReuse() {
        resetCellProperties()
    }
}

extension SchoolTableViewCell {
    func configure(_ school: SchoolViewModel, dataProvider: SchoolsDataProviding) {
        schoolNameLabel.text = school.schoolName
    }

    private func resetCellProperties() {
        schoolNameLabel.text = TableViewConstants.emptyString.rawValue
    }

    private func setUpView() {
        let labelContainer = DefaultContainerView(spacing: 5, identifier: .cellLabelContainer)
        contentView.addSubview(labelContainer)
        labelContainer.addArrangedSubview(schoolNameLabel)
        
        NSLayoutConstraint.activate([
            labelContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            labelContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            labelContainer.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            labelContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
        ])
    }
}
