//
//  LoadingIndicatorController.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import UIKit

class LoadingIndicatorController: UIViewController {
    private var loadingIndicator: UIActivityIndicatorView?
    private let containerView: UIView

    init() {
        self.containerView = UIView(frame: .zero)
        super.init(nibName: nil, bundle: nil)
        // Container SetUp
        view.addSubview(containerView)
        self.containerView.frame = view.bounds
        
        // Accessibility
        containerView.accessibilityIdentifier = "Loading Indicator"
        containerView.isAccessibilityElement = true
        containerView.accessibilityLabel = "Loading"

        // Loading Indicator View SetUp
        let loadingView = LoadingIndicatorView()
        self.loadingIndicator = loadingView
    }

    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let loadingIndicator = loadingIndicator else { return }
        containerView.addSubview(loadingIndicator)
        UIAccessibility.post(notification: .screenChanged, argument: containerView)
        loadingIndicator.startAnimating()
        loadingIndicator.center = .init(x: view.bounds.midX, y: view.bounds.midY)
    }
    override func viewWillDisappear(_ animated: Bool) {
        guard let loadingIndicator = loadingIndicator else { return }
        loadingIndicator.stopAnimating()
    }
}
