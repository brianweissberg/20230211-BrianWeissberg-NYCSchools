//
//  SchoolDetailsModel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

/// This data comes from a separate network call from school directory controller. The UUID connects this data to school directory data. If there is not school details data for a school in the directory, still show the school details view and present label to user that SAT information in not available for the selected school.
struct SchoolDetailsModel: Decodable {
    let uuid: String
    let numberOfTestTakers: String?
    let satCriticalReadingAverageScore: String?
    let satMathAverageScore: String?
    let satWritingAverageScore: String?

    enum CodingKeys: String, CodingKey {
        case uuid = "dbn"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAverageScore = "sat_critical_reading_avg_score"
        case satMathAverageScore = "sat_math_avg_score"
        case satWritingAverageScore = "sat_writing_avg_score"
    }
}
