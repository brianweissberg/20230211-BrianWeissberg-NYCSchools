//
//  SchoolDirectoryTableView.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/14/23.
//

import UIKit

class SchoolDirectoryTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        self.rowHeight = UITableView.automaticDimension
        self.translatesAutoresizingMaskIntoConstraints = false
        self.separatorStyle = .none
        self.backgroundColor = .schoolsListViewBackground
        self.register(SchoolTableViewCell.self, forCellReuseIdentifier: TableViewConstants.cell.rawValue)
    }
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
    
    func setUpConstraints(parentView: UIView) {
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: parentView.topAnchor),
            self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: parentView.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: parentView.trailingAnchor),
        ])
    }
}
