//
//  TestHelper.swift
//  20230211-BrianWeissberg-NYCSchoolsTests
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

enum DirectoryMockJson: String {
    case successSchoolsDirectory = "SuccessSchoolsDirectory"
    case empty = "Empty"
    case malformed = "Malformed"
}

enum DetailsMockJson: String {
    case successSchoolsDetails = "SuccessSchoolsDetails"
    case empty = "Empty"
    case malformed = "Malformed"
}

/// Handles mock JSON used for unit testing
class TestHelper {
    static func fetchDirectoryMockJson(_ mock: DirectoryMockJson) -> String? {
        var resource = ""
        switch mock {
        case .successSchoolsDirectory:
            resource = DirectoryMockJson.successSchoolsDirectory.rawValue
        case .empty:
            resource = DirectoryMockJson.empty.rawValue
        case .malformed:
            resource = DirectoryMockJson.malformed.rawValue
        }
        return fetchJsonContents(resource)
    }

    static func fetchDetailsMockJson(_ mock: DetailsMockJson) -> String? {
        var resource = ""
        switch mock {
        case .successSchoolsDetails:
            resource = DetailsMockJson.successSchoolsDetails.rawValue
        case .empty:
            resource = DetailsMockJson.empty.rawValue
        case .malformed:
            resource = DetailsMockJson.malformed.rawValue
        }
        return fetchJsonContents(resource)
    }

    private static func fetchJsonContents(_ resourceName: String) -> String? {
        guard let fileURL = Bundle.main.url(forResource: resourceName, withExtension: "json"), let contents = try? String(contentsOf: fileURL) else {
            return nil
        }
        return contents
    }
}
