//
//  Extensions.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import UIKit

/// Defines colors for custom display that works well when dark mode is on/off.
extension UIColor {
    static var schoolsListViewBackground: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .darkGray
            } else {
                return .white
            }
        }
    }
    
    static var animatorColor: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .white
            } else {
                return .darkGray
            }
        }
    }
    
    static var schoolsDetailViewBackground: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .black
            } else {
                return .white
            }
        }
    }
    
    static var schoolsDetailViewText: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .white
            } else {
                return .black
            }
        }
    }
    
    static var schoolsListViewText: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .white
            } else {
                return .black
            }
        }
    }
    
    static var schoolDetailPhoneNumber: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .systemBlue
            } else {
                return .systemBlue
            }
        }
    }

    static var schoolDirectoryTableView: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .black
            } else {
                return .white
            }
        }
    }

    static var schoolDirectoryNavigationBar: UIColor {
        return UIColor { (trailCollection: UITraitCollection) -> UIColor in
            if trailCollection.userInterfaceStyle == .dark {
                return .black
            } else {
                return .white
            }
        }
    }
}
