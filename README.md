# New York City Schools - iOS Application

Display names of schools in New York City. When user clicks on name of school, user can view details of school such as an overview of the school, SAT scores, extracurricular activities, contact information and map of school location. 

## Contact Information

[Connect With Me On LinkedIn](https://www.linkedin.com/in/brianweissberg/)

[View My Resume](https://drive.google.com/file/d/1O-jICEyKEEPOYnY7UMpFaHyCZCGrhqjd/view)

## Documentation

### Video Walkthrough

[![IMAGE ALT TEXT](https://www.seekpng.com/png/detail/71-713714_youtube-play-button-png-transparent-youtube-logo-300.png)](http://www.youtube.com/watch?v=mlR1e43Drrg "New York Schools - iOS Demo App")

* [0:00 Walkthrough](https://youtu.be/mlR1e43Drrg)
* [00:59 Network Error](https://youtu.be/mlR1e43Drrg&t=0h57s)
* [1:16 Accessibility](https://youtu.be/mlR1e43Drrg&t=1m18s)

Dark Mode Off            |  Dark Mode On
:-------------------------:|:-------------------------:|
![alt text](./MarkdownResources/RetryLight.PNG) | ![alt text](./MarkdownResources/RetryDark.PNG)
![alt text](./MarkdownResources/LoadingLight.PNG) | ![alt text](./MarkdownResources/LoadingDark.PNG)
![alt text](./MarkdownResources/DirectoryLight.PNG) | ![alt text](./MarkdownResources/DirectoryDark.PNG)
![alt text](./MarkdownResources/SearchBar_Light.PNG) | ![alt text](./MarkdownResources/SearchBar_Dark.PNG)
![alt text](./MarkdownResources/DetailsLight_1.PNG) | ![alt text](./MarkdownResources/DetailsDark_1.PNG)
![alt text](./MarkdownResources/DetailsLight_2.PNG) | ![alt text](./MarkdownResources/DetailsDark_2.PNG)

Email           |  Phone
:-------------------------:|:-------------------------:|
![alt text](./MarkdownResources/Email.PNG) | ![alt text](./MarkdownResources/Phone.PNG)