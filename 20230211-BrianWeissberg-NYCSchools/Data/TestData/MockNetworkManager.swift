//
//  MockNetworkManager.swift
//  20230211-BrianWeissberg-NYCSchoolsTests
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

enum MockNetworkError: LocalizedError {
    case failedNetwork
    
    var localizedDescription: String {
        switch self {
        case .failedNetwork:
            return "Failed Network Error from Mock Network Manager"
        }
    }
}

/// Mocks network calls for  logical unit tests and UI tests. Code for this manager is found in the production code for testing purposes only so that the same app can be used with real and mock network calls.
class MockNetworkManager: NetworkManaging {
    var testConfiguration: TestConfiguration
    
    init(testConfiguration: TestConfiguration) {
        self.testConfiguration = testConfiguration
    }
}

extension MockNetworkManager {
    func fetchData(_ url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        // Simulate Failed Network Call
        if testConfiguration.networkCallShouldFail {
            DispatchQueue.main.asyncAfter(deadline: .now() + testConfiguration.delay) {
                let error = MockNetworkError.failedNetwork
                completion(.failure(error))
            }
            return
        }

        // Simulate fetching the json for either the directory or the details screen
        var json: String?
        if url.absoluteString == NetworkUrl.schoolDetails.rawValue {
            let mock = testConfiguration.detailsMockJson
            json = fetchDetailsMockJson(mock)
        } else if url.absoluteString == NetworkUrl.schoolDirectory.rawValue {
            let mock = testConfiguration.directoryMockJson
            json = fetchDirectoryMockJson(mock)
        }
        
        // Convert the JSON string to data and complete successfully with that data
        guard let json = json else { return }
        let data = Data(json.utf8)
        // Simulate Delay in Network Call
        DispatchQueue.main.asyncAfter(deadline: .now() + testConfiguration.delay) {
            completion(.success(data))
        }
    }

    private func fetchDirectoryMockJson(_ mock: DirectoryMockJson) -> String? {
        switch testConfiguration.directoryMockJson {
        case .successSchoolsDirectory:
            return TestHelper.fetchDirectoryMockJson(.successSchoolsDirectory)
        case .empty:
            return TestHelper.fetchDirectoryMockJson(.empty)
        case .malformed:
            return TestHelper.fetchDirectoryMockJson(.malformed)
        }
    }

    private func fetchDetailsMockJson(_ mock: DetailsMockJson) -> String? {
        switch testConfiguration.detailsMockJson {
        case .successSchoolsDetails:
            return TestHelper.fetchDetailsMockJson(.successSchoolsDetails)
        case .empty:
            return TestHelper.fetchDetailsMockJson(.empty)
        case .malformed:
            return TestHelper.fetchDetailsMockJson(.malformed)
        }
    }
}
