//
//  SchoolsViewModelTests.swift
//  20230211-BrianWeissberg-NYCSchoolsTests
//
//  Created by Brian Weissberg on 2/13/23.
//

@testable import NY_Schools
import XCTest

/// Validates data fetching for school directory and school details
class SchoolsViewModelTests: XCTestCase {
    
    /// Tests `SchoolsViewModel.fetchSchoolsData`. Once that data is fetched, count the number of successful fetched objects. Expect 2 successfully fetched objects.
    func testFetchSchoolsDataSuccessJson() {
        let testConfig = TestConfiguration(directoryMockJson: .successSchoolsDirectory)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager())
        viewModel.fetchSchoolsData {
            XCTAssertEqual(viewModel.dataProvider.schools.count, 2)
        }
    }
    
    /// Tests `SchoolsViewModel.fetchSchoolDetailsData`. Once that data is fetched, count the number of successful fetched objects. Expect 5 successfully fetched objects.
    func testFetchSchoolDetailsDataSuccessJson() {
        let testConfig = TestConfiguration(detailsMockJson: .successSchoolsDetails)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager())
        viewModel.fetchSchoolDetailsData() {
            XCTAssertEqual(viewModel.dataProvider.allSchoolsDetails.count, 5)
        }
    }
    
    /// Validates `SchoolsViewModel.fetchSchoolsData` malformed JSON does not return any objects and also calls `SchoolViewModelDelegate.parseSchoolsFailure`
    func testFetchSchoolsDataMalformedJson() {
        let testConfig = TestConfiguration(directoryMockJson: .malformed)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let mockDelegate = SchoolsViewModelMockDelegate()
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager(), delegate: mockDelegate)
        viewModel.fetchSchoolsData {
            self.wait(for: [mockDelegate.expectation], timeout: 2.0)
            XCTAssertEqual(viewModel.dataProvider.schools.count, 0)
        }
    }
    
    /// Validates `SchoolsViewModel.fetchSchoolDetailsData` malformed JSON does not return any objects and also calls `SchoolViewModelDelegate.parseSchoolsFailure`
    func testFetchSchoolDetailsDataMalformedJson() {
        let testConfig = TestConfiguration(detailsMockJson: .malformed)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let mockDelegate = SchoolsViewModelMockDelegate()
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager(), delegate: mockDelegate)
        viewModel.fetchSchoolDetailsData {
            self.wait(for: [mockDelegate.expectation], timeout: 2.0)
            XCTAssertEqual(viewModel.dataProvider.schools.count, 0)
        }
    }
    
    /// Validates `SchoolsViewModel.fetchSchoolsData` empty JSON does not return any objects and also calls `SchoolViewModelDelegate.parseSchoolsFailure`
    func testFetchSchoolsDataEmptyJson() {
        let testConfig = TestConfiguration(directoryMockJson: .empty)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let mockDelegate = SchoolsViewModelMockDelegate()
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager(), delegate: mockDelegate)
        viewModel.fetchSchoolsData {
            self.wait(for: [mockDelegate.expectation], timeout: 2.0)
            XCTAssertEqual(viewModel.dataProvider.schools.count, 0)
        }
    }
    
    /// Validates `SchoolsViewModel.fetchSchoolDetailsData` empty JSON does not return any objects and also calls `SchoolViewModelDelegate.parseSchoolsFailure`
    func testFetchSchoolDetailsDataEmptyJson() {
        let testConfig = TestConfiguration(detailsMockJson: .empty)
        let mockNetworkManager = MockNetworkManager(testConfiguration: testConfig)
        let mockDelegate = SchoolsViewModelMockDelegate()
        let dataProvider = SchoolsDataProvider(networkManager: mockNetworkManager)
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: mockNetworkManager, serviceManager: SchoolsServiceManager(), delegate: mockDelegate)
        viewModel.fetchSchoolDetailsData {
            self.wait(for: [mockDelegate.expectation], timeout: 2.0)
            XCTAssertEqual(viewModel.dataProvider.schools.count, 0)
        }
    }
}
