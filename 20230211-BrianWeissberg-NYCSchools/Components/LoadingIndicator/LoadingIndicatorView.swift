//
//  LoadingIndicatorView.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import UIKit

class LoadingIndicatorView: UIActivityIndicatorView {
    init() {
        super.init(frame: .zero)
        self.style = .large
        self.color = .animatorColor
        self.translatesAutoresizingMaskIntoConstraints = false
        self.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        self.startAnimating()
    }
    
    required init(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}
