//
//  SchoolViewModel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

/// Contains data fetched from https://data.cityofnewyork.us/resource/s3k6-pzi2
struct SchoolViewModel {
    let uuid: String
    let schoolName: String
    let overview: String?
    let phoneNumber: String?
    let faxNumber: String?
    let email: String?
    let website: String?
    var latitude: Double?
    var longitude: Double?
    let extracurricularActivities: String?
    
    init(school: SchoolModel) {
        self.uuid = school.uuid
        self.schoolName = school.schoolName
        self.overview = school.overview
        self.phoneNumber = school.phoneNumber
        self.faxNumber = school.faxNumber
        self.email = school.email
        self.website = school.website
        self.extracurricularActivities = school.extracurricularActivities
        
        if let latitude = school.latitude, let latitudeDouble = Double(latitude) {
            self.latitude = latitudeDouble
        }
        if let longitude = school.longitude, let longitudeDouble = Double(longitude) {
            self.longitude = longitudeDouble
        }
    }
}
