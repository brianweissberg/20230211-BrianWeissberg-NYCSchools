//
//  SchoolsDataProvider.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

protocol SchoolsDataProviding {
    // Initial list of all schools. This list is never changed or modified.
    var allSchools: [SchoolViewModel] { get }
    // Current list of schools displayed in School Directory. This list may be different from all schools if users filters the schools in the search bar.
    var allSchoolsDidFinishLoading: Bool { get }
    // Current list of all schools being displayed to the user. This list changes based on input from the search bar.
    var schools: [SchoolViewModel] { get }
    // Details of all schools. When user taps on school in directory, search school details to see if we have detail information for that school. Used dictionary here for better performance.
    var allSchoolsDetails: [String: SchoolDetailsViewModel] { get }
    // Fetch list of all schools.
    func handleSchools(_ schools: [SchoolModel], completion: () -> Void)
    // Fetch details for individual schools.
    func handleSchoolDetails(_ details: [SchoolDetailsModel])
    // Filter schools when users uses search bar on School Directory.
    func filterSchools(_ str: String, completion: () -> Void)
}

class SchoolsDataProvider: SchoolsDataProviding {
    private(set) var schools: [SchoolViewModel]
    private(set) var allSchools: [SchoolViewModel]
    private(set) var allSchoolsDetails: [String: SchoolDetailsViewModel]
    private(set) var allSchoolsDidFinishLoading: Bool
    let networkManager: NetworkManaging
    
    init(networkManager: NetworkManaging = NetworkManager()) {
        self.schools = [SchoolViewModel]()
        self.allSchools = [SchoolViewModel]()
        self.allSchoolsDetails = [String: SchoolDetailsViewModel]()
        self.allSchoolsDidFinishLoading = false
        self.networkManager = networkManager
    }

    func handleSchools(_ schools: [SchoolModel], completion: () -> Void) {
        let mapped = schools.map { SchoolViewModel(school: $0) }
        let sorted = mapped.sorted(by: { $0.schoolName < $1.schoolName } )
        self.schools = sorted
        self.allSchools = sorted
        self.allSchoolsDidFinishLoading = true
        completion()
    }

    func handleSchoolDetails(_ details: [SchoolDetailsModel]) {
        let mapped = details.map { SchoolDetailsViewModel(details: $0) }
        mapped.forEach { schoolDetails in
            allSchoolsDetails[schoolDetails.uuid] = schoolDetails
        }
    }

    func filterSchools(_ str: String, completion: () -> Void) {
        if str.isEmpty {
            schools = allSchools
            completion()
            return
        }
        
        schools = []
        var result = [SchoolViewModel]()
        let lowercasedFilter = str.lowercased()
        for school in allSchools {
            let schoolName = school.schoolName.lowercased()
            if schoolName.contains(lowercasedFilter) {
                result.append(school)
            }
        }
        schools = result
        completion()
    }
}
