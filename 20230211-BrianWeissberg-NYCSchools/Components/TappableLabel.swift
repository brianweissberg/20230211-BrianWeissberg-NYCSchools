//
//  TappableLabel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import Foundation
import UIKit

class TappableLabel: DefaultLabel {
    private let tappableString: String
    private let type: LabelType
    init(type: LabelType, tappableString: String, identifier: DefaultLabelAccessibliltyIdentifiers) {
        self.tappableString = tappableString
        self.type = type
        super.init(textColor: .schoolDetailPhoneNumber, identifier: identifier)
        self.text = tappableString
        self.setUpGestureRecognizer()
    }
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}

extension TappableLabel {
    @objc func buttonTapped() {
        switch type {
        case .phone:
            let phoneUrl = createTappableUrl(type: .phone, urlString: tappableString)
            openUrl(phoneUrl)
        case .email:
            let mailUrl = createTappableUrl(type: .email, urlString: tappableString)
            openUrl(mailUrl)
        }
    }
    
    private func openUrl(_ url: URL?) {
        guard let url = url,
              UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
    
    private func filterPhoneDigits(_ number: String) -> String {
        let filtredUnicodeScalars = number.unicodeScalars.filter { CharacterSet.decimalDigits.contains($0) }
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }

    private func createTappableUrl(type: LabelType, urlString: String) -> URL? {
        switch type {
        case .phone:
            let filtered = filterPhoneDigits(urlString)
            guard let phoneNumber = URL(string: Constants.telephoneScheme.rawValue + filtered) else { return nil }
            return phoneNumber
        case .email:
            guard let email = URL(string: Constants.mailTo.rawValue + urlString) else { return nil }
            return email
        }
    }

    private func setUpGestureRecognizer() {
        isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonTapped))
        addGestureRecognizer(gestureRecognizer)
    }

    private enum Constants: String {
        case telephoneScheme = "tel://"
        case mailTo = "mailto:"
    }

    enum LabelType {
        case phone
        case email
    }
}
