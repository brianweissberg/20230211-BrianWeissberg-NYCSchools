//
//  DefaultContainerView.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import UIKit

class DefaultContainerView: UIStackView {
    init(axis: NSLayoutConstraint.Axis = .vertical,
         spacing: CGFloat = 0,
         alignment: UIStackView.Alignment = .leading,
         identifier: DefaultContainerAccessibliltyIdentifiers) {
        super.init(frame: .zero)
        self.axis = axis
        self.spacing = spacing
        self.alignment = alignment
        self.translatesAutoresizingMaskIntoConstraints = false
        self.distribution = .fill
        self.accessibilityIdentifier = identifier.rawValue
    }
    
    required init(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}
