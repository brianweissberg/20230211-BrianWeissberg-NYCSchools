//
//  SchoolDirectorySearchBarController.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/13/23.
//

import UIKit

class SchoolDirectorySearchBarController: UISearchController {
    init(controllerDelegate: UISearchControllerDelegate, searchBarDelegate: UISearchBarDelegate, searchResultsUpdater: UISearchResultsUpdating, controller: UIViewController) {
        super.init(nibName: nil, bundle: nil)
        self.searchResultsUpdater = searchResultsUpdater
        self.automaticallyShowsCancelButton = true
        self.obscuresBackgroundDuringPresentation = false
        self.searchBar.autocapitalizationType = .none
        self.searchBar.searchTextField.placeholder = NSLocalizedString(SchoolConstants.searchBarPrompt.rawValue, comment: "")
        self.searchBar.returnKeyType = .done
        self.delegate = controllerDelegate
        self.searchBar.delegate = searchBarDelegate
        self.searchBar.accessibilityIdentifier = SchoolDirectoryAccessibiltyIdentifiers.searchBar.rawValue
    }
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
}
