//
//  SchoolModel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

/// Contains all school information *except* information about SAT information that is fetched in school details network call. The UUID of this model maps to the UUID in school details so that SAT information can be shown to the user if it is available.
struct SchoolModel: Decodable {
    let uuid: String
    let schoolName: String
    let overview: String?
    let phoneNumber: String?
    let faxNumber: String?
    let email: String?
    let website: String?
    let latitude: String?
    let longitude: String?
    let extracurricularActivities: String?

    enum CodingKeys: String, CodingKey {
        case uuid = "dbn"
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case email = "school_email"
        case website
        case extracurricularActivities = "extracurricular_activities"
        case latitude
        case longitude
    }
}
