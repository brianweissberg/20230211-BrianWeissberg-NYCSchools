//
//  SchoolDetailsViewModel.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

/// Contains data fetched from https://data.cityofnewyork.us/resource/f9bf-2cp4.json
struct SchoolDetailsViewModel {
    let uuid: String
    let numberOfTestTakers: String?
    let satCriticalReadingAverageScore: String?
    let satMathAverageScore: String?
    let satWritingAverageScore: String?
    
    init(details: SchoolDetailsModel) {
        self.uuid = details.uuid
        self.numberOfTestTakers = details.numberOfTestTakers
        self.satCriticalReadingAverageScore = details.satCriticalReadingAverageScore
        self.satMathAverageScore = details.satMathAverageScore
        self.satWritingAverageScore = details.satWritingAverageScore
    }
}
