//
//  SchoolDetailsController.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation
import MapKit
import UIKit

/// Displays details of a school after school is selected from SchoolDirectoryController.tableView
class SchoolDetailsController: UIViewController {
    private let schoolViewModel: SchoolViewModel
    private let contentContainer: DefaultContainerView
    private let scrollViewContainer: DefaultContainerView
    private let mapViewContainer: DefaultContainerView
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .schoolsDetailViewBackground
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    init(school: SchoolViewModel, details: SchoolDetailsViewModel?) {
        self.scrollViewContainer = DefaultContainerView(identifier: .scrollViewContainer)
        self.mapViewContainer = DefaultContainerView(identifier: .mapViewContainer)
        self.contentContainer = DefaultContainerView(spacing: 20, identifier: .detailsContentContainer)
        self.schoolViewModel = school
        super.init(nibName: nil, bundle: nil)
        DispatchQueue.main.async {
            self.configureView(school, details)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError(TableViewConstants.initFatalError.rawValue)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // This is a little hacky but I kept getting view stuttering issues when adding the mapview in without the delay. Not sure what the issue is and didn't have time to debug it.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.addMapView(self.schoolViewModel)
        }
    }
    
    private func configureView(_ school: SchoolViewModel, _ details: SchoolDetailsViewModel?) {
        addEssentialContainersAndConfigureConstraints()
        addSchoolNameAndOverview(school)
        addSatInformation(details)
        addExtracurricularActivities(school)
        addContactDetails(school)
    }
}

extension SchoolDetailsController {
    private func addEssentialContainersAndConfigureConstraints() {
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        scrollView.addSubview(scrollViewContainer)
        NSLayoutConstraint.activate([
            scrollViewContainer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            scrollViewContainer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            scrollViewContainer.topAnchor.constraint(equalTo: scrollView.topAnchor),
            scrollViewContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            scrollViewContainer.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
        
        scrollViewContainer.addSubview(contentContainer)
        NSLayoutConstraint.activate([
            contentContainer.leadingAnchor.constraint(equalTo: scrollViewContainer.leadingAnchor, constant: 15),
            contentContainer.trailingAnchor.constraint(equalTo: scrollViewContainer.trailingAnchor, constant: -15),
            contentContainer.topAnchor.constraint(equalTo: scrollViewContainer.topAnchor),
        ])
        
        scrollViewContainer.addSubview(mapViewContainer)
        NSLayoutConstraint.activate([
            mapViewContainer.leadingAnchor.constraint(equalTo: scrollViewContainer.leadingAnchor),
            mapViewContainer.trailingAnchor.constraint(equalTo: scrollViewContainer.trailingAnchor),
            mapViewContainer.bottomAnchor.constraint(equalTo: scrollViewContainer.bottomAnchor),
            mapViewContainer.topAnchor.constraint(equalTo: contentContainer.bottomAnchor, constant: 20),
            mapViewContainer.heightAnchor.constraint(equalToConstant: view.frame.width * 0.7)
        ])
    }
    
    private func addSchoolNameAndOverview(_ school: SchoolViewModel) {
        // Container
        let schoolNameContainer = DefaultContainerView(identifier: .detailsSchoolNameContainer)
        schoolNameContainer.isLayoutMarginsRelativeArrangement = true
        schoolNameContainer.isAccessibilityElement = true
        schoolNameContainer.becomeFirstResponder()
        schoolNameContainer.layoutMargins = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        contentContainer.addArrangedSubview(schoolNameContainer)
        
        // School Name Label
        let schoolNameLabel = DefaultLabel(font: .boldSystemFont(ofSize: 25), textColor: .schoolsDetailViewText, identifier: .detailsSchoolNameLabel)
        schoolNameLabel.text = school.schoolName
        schoolNameContainer.addArrangedSubview(schoolNameLabel)

        // School Overview Container
        guard let overview = school.overview else { return }
        let overviewContainer = DefaultContainerView(identifier: .detailsOverviewContainer)
        overviewContainer.isLayoutMarginsRelativeArrangement = true
        overviewContainer.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        schoolNameContainer.addArrangedSubview(overviewContainer)

        // School Overview Label
        let overviewLabel = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsOverviewLabel)
        overviewLabel.text = overview
        overviewContainer.addArrangedSubview(overviewLabel)
        
        // Accessibility
        schoolNameContainer.accessibilityLabel = school.schoolName + AccessibilityHelper.commaPause.rawValue + overview
    }

    private func addSatInformation(_ details: SchoolDetailsViewModel?) {
        // Container
        let satLabelsContainer = DefaultContainerView(identifier: .detailsSatLabelsContainer)
        contentContainer.addArrangedSubview(satLabelsContainer)
        // If the school does not have any SAT details, display that information in the title label
        let label = DefaultLabel(font: .boldSystemFont(ofSize: 20), textColor: .schoolsDetailViewText, identifier: .detailsSatDetailsTitleLabel)
        let labelTitle = SchoolConstants.satDetails.rawValue
        label.text = labelTitle
        satLabelsContainer.addArrangedSubview(label)
        // Accessibility
        satLabelsContainer.isAccessibilityElement = true
        var accessibilityLabelText = labelTitle
        guard let details = details else {
            label.text = SchoolConstants.noSATDetails.rawValue
            return
        }
        
        // Number of Test Takers
        if let numberOfTestTakers = details.numberOfTestTakers {
            let label = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsNumberTestTakersLabel)
            let labelText = SchoolDetailsConstants.testTakers.rawValue + numberOfTestTakers
            label.text = labelText
            accessibilityLabelText += AccessibilityHelper.commaPause.rawValue + labelText
            satLabelsContainer.addArrangedSubview(label)
        }
        
        // Reading Score
        if let readingScore = details.satCriticalReadingAverageScore {
            let label = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsReadingScoreLabel)
            let labelText = SchoolDetailsConstants.averageReadingScore.rawValue + readingScore
            label.text = labelText
            accessibilityLabelText += AccessibilityHelper.commaPause.rawValue + labelText
            satLabelsContainer.addArrangedSubview(label)
        }
        
        // Writing Score
        if let writingScore = details.satWritingAverageScore {
            let label = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsWritingScoreLabel)
            let labelText = SchoolDetailsConstants.averageWritingScore.rawValue + writingScore
            label.text = labelText
            accessibilityLabelText += AccessibilityHelper.commaPause.rawValue + labelText
            satLabelsContainer.addArrangedSubview(label)
        }
        
        // Math Score
        if let mathScore = details.satMathAverageScore {
            let label = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsMathScoreLabel)
            let labelText = SchoolDetailsConstants.averageMathScore.rawValue + mathScore
            label.text = labelText
            accessibilityLabelText += AccessibilityHelper.commaPause.rawValue + labelText
            satLabelsContainer.addArrangedSubview(label)
        }
        satLabelsContainer.accessibilityLabel = accessibilityLabelText
    }
    
    private func addExtracurricularActivities(_ school: SchoolViewModel) {
        guard let extracurricularActivities = school.extracurricularActivities else { return }
        // Container
        let extracurricularActivitiesContainer = DefaultContainerView(spacing: 5, identifier: .detailsExtracurricularActivitiesContainer)
        contentContainer.addArrangedSubview(extracurricularActivitiesContainer)
        
        // Extracurricular Activities Title
        let extracurricularActivitiesTitleLabel = DefaultLabel(font: .boldSystemFont(ofSize: 20), textColor: .schoolsDetailViewText, identifier: .detailsExtracurricularActivitiesLabel)
        let titleLabelText = SchoolConstants.extracurricularActivities.rawValue
        extracurricularActivitiesTitleLabel.text = titleLabelText
        extracurricularActivitiesContainer.addArrangedSubview(extracurricularActivitiesTitleLabel)
        
        // Extracurricular Activities Label
        let extracurricularActivitiesLabel = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsExtracurricularActivitiesLabel)
        extracurricularActivitiesLabel.text = extracurricularActivities
        extracurricularActivitiesContainer.addArrangedSubview(extracurricularActivitiesLabel)
        
        // Accessibility
        extracurricularActivitiesContainer.isAccessibilityElement = true
        let containerAccessibilityText = titleLabelText + AccessibilityHelper.commaPause.rawValue + extracurricularActivities
        extracurricularActivitiesContainer.accessibilityLabel = containerAccessibilityText
    }
    
    private func addContactDetails(_ school: SchoolViewModel) {
        // Make sure that at least one of these exists or else do not add an empty view to main container
        guard (school.phoneNumber != nil) ||
                (school.faxNumber != nil) ||
                (school.email != nil) ||
                (school.website != nil) else { return }
        
        // Container
        let contactContainer = DefaultContainerView(spacing: 5, identifier: .detailsContactContainer)
        contentContainer.addArrangedSubview(contactContainer)
        
        // Contact Title Label
        let contactTitleLabel = DefaultLabel(font: .boldSystemFont(ofSize: 20), textColor: .schoolsDetailViewText, identifier: .detailsContactTitleLabel)
        contactTitleLabel.text = SchoolConstants.contact.rawValue
        contactContainer.addArrangedSubview(contactTitleLabel)
        
        // Phone Number
        if let phoneNumber = school.phoneNumber {
            let phoneLabel = TappableLabel(type: .phone, tappableString: phoneNumber, identifier: .detailsPhoneLabel)
            contactContainer.addArrangedSubview(phoneLabel)
        }
        
        // Fax Number
        if let faxNumber = school.faxNumber {
            let faxLabel = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsFaxLabel)
            faxLabel.text = SchoolDetailsConstants.fax.rawValue + faxNumber
            contactContainer.addArrangedSubview(faxLabel)
        }
        
        // School Email
        if let schoolEmail = school.email {
            let emailLabel = TappableLabel(type: .email, tappableString: schoolEmail, identifier: .detailsSchoolEmailLabel)
            contactContainer.addArrangedSubview(emailLabel)
        }
        
        // School Website
        if let schoolWebsite = school.website {
            let label = DefaultLabel(textColor: .schoolsDetailViewText, identifier: .detailsSchoolWebsiteLabel)
            label.text = SchoolDetailsConstants.website.rawValue + schoolWebsite
            contactContainer.addArrangedSubview(label)
        }
    }
    
    private func addMapView(_ school: SchoolViewModel) {
        guard let latitude = school.latitude,
              let longitude = school.longitude else { return }
        let mapView = SchoolDetailsMapView(schoolName: school.schoolName, latitude: latitude, longitude: longitude)
        mapViewContainer.addSubview(mapView)
        mapViewContainer.heightAnchor.constraint(equalToConstant: view.frame.width * 0.7).isActive = true
        mapView.setUpConstraints(parentView: mapViewContainer)
        
        // Accessibility
        mapViewContainer.isAccessibilityElement = true
        let mapViewContainerAccessibilityLabel = AccessibilityHelper.mapViewLabel.rawValue + AccessibilityHelper.commaPause.rawValue + school.schoolName
        mapViewContainer.accessibilityLabel = mapViewContainerAccessibilityLabel
    }
}
