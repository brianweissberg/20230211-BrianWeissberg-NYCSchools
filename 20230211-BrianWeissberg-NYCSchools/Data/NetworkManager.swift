//
//  NetworkManager.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import Foundation

enum HTTPStatusCode: Int {
    case ok = 200
}

protocol NetworkManaging {
    func fetchData(_ url: URL, completion: @escaping (Result<Data, Error>) -> Void)
}

/// Generic networking class for fetching data
class NetworkManager: NetworkManaging {
    func fetchData(_ url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == HTTPStatusCode.ok.rawValue  {
                completion(.success(data))
            }
        }
        task.resume()
    }
}
