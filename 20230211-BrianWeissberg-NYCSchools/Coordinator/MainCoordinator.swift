//
//  MainCoordinator.swift
//  20230211-BrianWeissberg-NYCSchools
//
//  Created by Brian Weissberg on 2/11/23.
//

import UIKit

typealias alertAction = () -> ()

protocol MainCoordinating {
    var navController: UINavigationController { get set }
    func showSchoolDirectory()
    func showSchoolDetails(school: SchoolViewModel, dataProvider: SchoolsDataProviding)
    func showRetryAlert(message: String, completion: @escaping alertAction)
    func removeSchoolDirectoryLoadingIndicator()
    func showSchoolDirectoryLoadingIndicator()
}

/// Coorinator handles the displaying and removing of views. This ensures that views do not have knowlege about other views and that they are injectable.
class MainCoordinator: MainCoordinating {
    var navController: UINavigationController
    private var schoolDirectoryController: SchoolDirectoryController?
    private var directoryLoadingIndicatorController: LoadingIndicatorController?
    private let testConfiguration: TestConfiguration

    init(navController: UINavigationController, testConfiguration: TestConfiguration) {
        self.navController = navController
        self.testConfiguration = testConfiguration
    }

    func showSchoolDirectory() {
        let dataProvider = SchoolsDataProvider()
        let networkManager: NetworkManaging = testConfiguration.isTesting ? MockNetworkManager(testConfiguration: testConfiguration) : NetworkManager()
        let viewModel = SchoolsViewModel(dataProvider: dataProvider, networkManager: networkManager)
        let controller = SchoolDirectoryController(viewModel: viewModel, coordinator: self)
        self.schoolDirectoryController = controller
        viewModel.delegate = controller
        navController.pushViewController(controller, animated: true)
    }

    func showSchoolDetails(school: SchoolViewModel, dataProvider: SchoolsDataProviding) {
        let details = dataProvider.allSchoolsDetails[school.uuid]
        let controller = SchoolDetailsController(school: school, details: details)
        navController.modalTransitionStyle = .crossDissolve
        navController.pushViewController(controller, animated: true)
    }

    func showRetryAlert(message: String, completion: @escaping alertAction) {
        let title = TableViewConstants.networkError.rawValue
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: TableViewConstants.retry.rawValue, style: .cancel) { _ in
            completion()
        }
        controller.addAction(action)
        navController.present(controller, animated: true)
    }

    func removeSchoolDirectoryLoadingIndicator() {
        directoryLoadingIndicatorController?.view.removeFromSuperview()
    }

    func showSchoolDirectoryLoadingIndicator() {
        guard directoryLoadingIndicatorController == nil else { return }
        let loadingIndicatorController = LoadingIndicatorController()
        self.directoryLoadingIndicatorController = loadingIndicatorController
        schoolDirectoryController?.view.addSubview(loadingIndicatorController.view)
    }
}
